# installation

you need to have `npm` installed

you will need to install bower and polymer-cli via npm

```
npm install -g bower
npm install -g polymer-cli
```

once that's done run:

```
bower i
```

in the project root.

# viewing

to view the polymer elements demo run:

```
polymer serve --open
```

and append the filename of the file you want to view in the url.


alternatively you can use webstorm's built-in view in browser function.
works just as well.